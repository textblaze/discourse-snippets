# name: discourse-snippet
# about: Enables Text Blaze Snippet preview in discourse
# version: 0.1.1
# authors: Scott Fortmann-Roe
# url: https://gitlab.com/textblaze/discourse-snippets

enabled_site_setting :snippet_enabled

register_svg_icon "sticky-note" if respond_to?(:register_svg_icon)
register_svg_icon "wand-magic-sparkles" if respond_to?(:register_svg_icon)
register_svg_icon "folder" if respond_to?(:register_svg_icon)
register_svg_icon "database" if respond_to?(:register_svg_icon)

# Allow users to delete their own topics in the Snippet Exchange
after_initialize do
  module ::TopicGuardian
    def can_delete_topic?(topic)
      !topic.trashed? &&
      # Direct messages have topic.category unset (NilClass)
      (is_staff? || (topic.category && topic.category.id == 6 && is_my_own?(topic))) &&
      !(topic.is_category_topic?) &&
      !Discourse.static_doc_topic_ids.include?(topic.id)
    end
  end
end
