import { withPluginApi } from 'discourse/lib/plugin-api';
import ComposerController from 'discourse/controllers/composer';


function initializeSnippetMenu(api) {
  api.addComposerToolbarPopupMenuOption({
    action: (toolbarEvent) => {
      toolbarEvent.applySurround(
        '[snippet]\n',
        '\n[/snippet]',
        'snippet_text',
        { multiline: false }
      );
    },
    icon: 'sticky-note',
    label: 'snippet.title',
  });

  api.addComposerToolbarPopupMenuOption({
    action: (toolbarEvent) => {
      toolbarEvent.applySurround(
        '[prompt]\n',
        '\n[/prompt]',
        'prompt_text',
        { multiline: false }
      );
    },
    icon: 'wand-magic-sparkles',
    label: 'prompt.title',
  });

  api.addComposerToolbarPopupMenuOption({
    action: (toolbarEvent) => {
      toolbarEvent.applySurround(
        '[space]\n',
        '\n[/space]',
        'space_text',
        { multiline: false }
      );
    },
    icon: 'database',
    label: 'space.title',
  });

  api.addComposerToolbarPopupMenuOption({
    action: (toolbarEvent) => {
      toolbarEvent.applySurround(
        '[bundle]\n',
        '\n[/bundle]',
        'bundle_text',
        { multiline: false }
      );
    },
    icon: 'folder',
    label: 'bundle.title',
  });
}

export default {
  name: 'apply-snippets',
  initialize(container) {
    const siteSettings = container.lookup('site-settings:main');
    if (siteSettings.snippet_enabled) {
      withPluginApi('1.15.0', initializeSnippetMenu);
    }
  }
};
