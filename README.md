# Discourse Snippets plugin

This plugin enables parsing of `[snippet][/snippet]`, and `[prompt][example_response][/example_response][/prompt]` BBCode blocks in the posts on [Text Blaze Community](https://community.blaze.today/). See an example parsed output in [this community post](https://community.blaze.today/t/october-development-update/22860).

## Local development

[Discourse Development Contributor Guidelines](https://meta.discourse.org/t/discourse-development-contributor-guidelines/3823)

Once you have Discourse up and running:

- Add this repo inside the `/plugins` folder of Discourse.
- Run `bin/ember-cli -u` to get Discourse started.
- To create an admin account, run `RAILS_ENV=development bundle exec rake admin:create`
- Go to `/admin/customize/themes/1` then "Edit CSS/HTML"
    - Click on "head" and add a link to the `engine.css`. It can be the one at prod `https://blaze.today/css/engine.css` or for local development, run it via the blog.
    - Click on "body" and add the `engine.js` file, for prod `https://blaze.today//js/engine.main.js`, or run it locally.

## Production deploy

Via GUI, visit the [webadmin panel](https://community.blaze.today/admin/upgrade)  
Via cmdline, `./launcher rebuild app` in `/var/discourse` working directory.

This takes a long time (~five minutes) because it rebuilds/minifies a bunch of ruby and JavaScript packages, etc.   
There does not seem to be a faster way to do this.
